<?php
namespace api\actions;

use Yii;
use yii\web\ServerErrorHttpException;
use yii\helpers\Url;
use yii\helpers\VarDumper;
use yii\data\ArrayDataProvider;
use yii\data\Sort;

class GasolinePrices extends \yii\rest\CreateAction
{
    /**
     * @inheritdoc
     */
    public function run()
    {
        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id);
        }
        /* @var $model \yii\db\ActiveRecord */
        $model = new $this->modelClass();
        
        $model->load(Yii::$app->getRequest()->getQueryParams(), '');
        
        $prices = $model->getPrices();
        $pc = $model->postal_code;
        if ($pc) {
            $resultData = array_filter($prices->results, function($v, $k) use ($pc){
                return $v->codigopostal == $pc;
            }, ARRAY_FILTER_USE_BOTH);
        }

        $sort = new Sort([
            'attributes' => [
                'regular' => [
                    'asc' => ['regular' => SORT_ASC],
                    'desc' => ['regular' => SORT_DESC],
                    'defaultOrder' => SORT_DESC,
                    'label' => 'regular',
                ],
                // or any other attribute
            ],
        ]);
        $provider = new ArrayDataProvider([
            'allModels' => $resultData,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => $sort
        ]);

        if(true){
        	$response = Yii::$app->getResponse();
            $response->setStatusCode(201);
            return $provider;
        } elseif (!$model->hasErrors()) {
            throw new ServerErrorHttpException('Failed to unknown reason.');
        }
    }
}
