<?php

namespace Frontend\models;

use Yii;
use yii\base\Model;
use yii\helpers\VarDumper;
use common\models\CodigosPostales; 

/**
 * GasolinePRice es el modelo para obtener los datos de la gasolina por ubicacion geografica.
 */
class GasolinePrice extends Model
{
    public $state;
    public $town;
    public $order;
    public $postal_code;
    Const ASC = 1;
    Const DESC = 2;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [
            	[
            		'state', 
            	], 
            	'required',
            	'message' => 'Favor de seleccionar un estado',
                'when' => function($model, $attribute) {
                    return false;
                },
                'whenClient' => "function (attribute, value) {
                    value = $('#gasolineprice-postal_code').val();
                    if(value){
                        return false;
                    } else {
                        return true;
                    }
                }",
            ],
            [
            	[
            		'town'
            	], 
            	'required',
            	'message' => 'Favor de seleccionar un municipio',
                'when' => function($model, $attribute) {
                    return false;
                },
                'whenClient' => "function (attribute, value) {
                    value = $('#gasolineprice-postal_code').val();
                    if(value){
                        return false;
                    } else {
                        return true;
                    }
                }",
            ],
            [
                [
                    'order'
                ], 
                'required',
                'message' => 'Favor de seleccionar un orden'
            ],
            [['state', 'town', 'order'], 'integer'],

            [
                [
                    'postal_code'
                ], 
                "string",
                'length' => [4, 5],
                'tooLong' => 'Código postal es de maximo 5 caracteres',
                'tooShort' => 'Código postal debe tener almeno 4 caracteres'
            ],
            [['postal_code',],
                'required',
                'message' => 'Favor de escribir un código postal.',
                'when' => function($model, $attribute) {
                    return false;
                },
                'whenClient' => "function (attribute, value) {
                    value = $('#gasolineprice-state').val();
                    if(value){
                        return false;
                    } else {
                        return true;
                    }
                }",
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'state' => 'Estado',
            'town'  => 'Municipio',
            'order' => 'Orden',
            'postal_code' => 'Código postal',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getOrderArray()
    {
        return [
        	self::DESC => "Descendente",
        	self::ASC => "Ascendente",
        ];
    }

    public function getPrices($model)
    {        
        if ($model->postal_code) {
            $pc = $model->postal_code;
        } else {
            $postalCode = CodigosPostales::find()
            ->andWhere([
                "c_estado" => $model->state,
                "c_mnpio" => $model->town    
            ])
            ->one();
            $pc = $postalCode->codigo_postal;
        }
        //$pc = "63194";
        $attributeOrder = ($model->order == 1 )?"regular":"-regular";
        $url = Yii::$app->params['urlApi'] . "gasoline-prices?postal_code=".$pc."&sort=".$attributeOrder;
    	$curl = curl_init();
    	curl_setopt_array($curl, [
		    CURLOPT_RETURNTRANSFER => 1,
		    CURLOPT_URL => $url,
		    CURLOPT_USERAGENT => 'Codular Sample cURL Request'
		]);

        $response = curl_exec($curl);
        $error = curl_error($curl);        
        curl_close($curl);
        if ($error) {
           return $error;
        }
        return $response;
    }
}