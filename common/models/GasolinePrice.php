<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\helpers\VarDumper;

/**
 * GasolinePRice es el modelo para obtener los datos de la gasolina por ubicacion geografica.
 */
class GasolinePrice extends Model
{
    public $postal_code;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [
            	[
            		'postal_code', 
            	], 
            	'required',
            ],
            [['postal_code'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'postal_code' => 'Codigo Postal',
        ];
    }

    public static function getPrices()
    {
        $url = "https://api.datos.gob.mx/v1/precio.gasolina.publico";
    	$curl = curl_init();
        curl_setopt($curl,CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl,CURLOPT_SSL_VERIFYHOST,false);
        curl_setopt($curl,CURLOPT_SSL_VERIFYPEER,false);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HEADER, 0);
        $response = curl_exec($curl);
        $response = json_decode($response);
        $error = curl_error($curl);

        curl_close($curl);
        if ($error) {
           return false;
        }
        return $response;
    }
}
