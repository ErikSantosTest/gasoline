## Instalar yii2 Framework

 Instalar yii2 framework [Guía](https://www.yiiframework.com/doc/guide/2.0/es/start-installation).

---

## Descargar base de datos

 En la raíz de este repositorio se encuentra el archivo sepomex.sql importar en MySQL.

---

## Clonar el proyecto

 Clonar el proyecto tu carpeta pública. "git clone git@bitbucket.org:ErikSantosTest/gasoline.git"

---

## Configurar proyecto
 
 1. Ejecutar yii2 en ambiente de desarrollo. "./yii init"
 2. Actualizar las librerias "composer update"
 3. Configurar base de datos en el archivo "common/config/main-local"

---

## Ejecutar la url

localhost/proyecto/index.php?r=gasoline-price%2Fcreate

