<?php

namespace common\models;

class CodigosPostales extends \yii\db\ActiveRecord
{   

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'codigos_postales';
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlazaEstado()
    {
        return $this->hasMany(
            PlazaMunicipio::class, 
            ['estado_id' => 'c_estado']
        );
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlazaMunicipio()
    {
        return $this->hasMany(
            PlazaMunicipio::class, 
            ['municipio_id' => 'c_mnpio']
        );
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAsociacionPlazas()
    {
        return $this->hasMany(
            PlazaMunicipio::class, 
            ['municipio_id' => 'c_mnpio']
        )->onCondition(['estado_id' => $this->c_estado]);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlazas()
    {
        return $this->hasMany(
            \api\models\Plaza::class, [
                'id' => 'plaza_id'
            ]
        )->via("asociacionPlazas");
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlaza()
    {
        return $this->hasOne(
            \api\models\Plaza::class, [
                'id' => 'plaza_id'
            ]
        )->via("asociacionPlazas");
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoPostalChar()
    {
        return strlen($this->codigo_postal) < 5
            ? "0" . $this->codigo_postal
            : (string)$this->codigo_postal;
    }
}