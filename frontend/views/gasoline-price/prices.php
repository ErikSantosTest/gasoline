<?php
?>

<?php if(empty($prices)){ ?>
<div class="alert alert-info alert-dismissible">
  	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
  	<strong>No se encotraron resultados con la información proporcionada</strong>
</div>
<?php }else{ ?>
<div class="row" style="margin-bottom:25px;width:fit-content;margin-left:auto;margin-right:auto;">
	<div class="col-md-12">
		<?php echo  $map->display(); ?>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<table class="table boostrap">
		  <thead class="thead-dark">
		    <tr>
		      <th scope="col">#</th>
		      <th scope="col">Nombre</th>
		      <th scope="col">Dirección</th>
		      <th scope="col">$ Regular</th>
		      <th scope="col">$ Premium</th>
		      <th scope="col">$ Diesel</th>
		    </tr>
		  </thead>
		  <tbody>
		  	<?php foreach ($prices  as $key => $price) { ?>
		    	<tr>
					<th scope="row"><?php echo $key+1?></th>
					<td><?php echo $price["razonsocial"]; ?></td>
					<td><?php echo $price["calle"]; ?></td>
					<td><?php echo $price["regular"]; ?></td>
					<td><?php echo $price["premium"]; ?></td>
					<td><?php echo $price["dieasel"]; ?></td>
		    	</tr>
			<?php } ?>
		  </tbody>
		</table>
	</div>
</div>
<?php } ?>