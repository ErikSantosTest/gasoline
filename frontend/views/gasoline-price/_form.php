<?php

use common\models\CodigosPostales; 
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\View;
use yii\helpers\VarDumper;
use yii\widgets\ActiveForm;

$states = ArrayHelper::map(
    CodigosPostales::find()
        ->select(["c_estado", "estado"])
        ->distinct()
        ->all(),
    'c_estado', 
    'estado'
);

$urlTownIst = Yii::$app->urlManager->createUrl('gasoline-price/town-list');

$this->registerJs(<<<JS
jQuery(document).ready(function($) {   
   $("#form-gasoline-prices").submit(function(event) {
        event.preventDefault();
        var url = $(this).attr('action');
        var data = $(this).serializeArray();
        $.ajax({
            url: url,
            type: 'post',
            //dataType: 'json',
            data: data
        })
        .done(function(response) {
        	$("#table_prices").html(response)
        })
        .fail(function(xhr, status, error) {
  			alert(error + "d");
        });
    
    });
});

$("#gasolineprice-postal_code").change(function() {
	if($(this).val != ""){
    	$("#gasolineprice-state").val("");
    	$("#gasolineprice-town").val("");
	}
});

$("#gasolineprice-state, #gasolineprice-town").change(function() {
	if($(this).val != ""){
    	$("#gasolineprice-postal_code").val("");
	}
});
JS
, View::POS_END);


$this->registerCss(
	"#gmap0-map-canvas{width: 50%;height: 300px;}"
);

?>

<?php 
	$form = ActiveForm::begin(
		['id' => 'form-gasoline-prices', 'action' => ['render']]
	); 
?>
	<div class="row" id="form-gasoline-prices" style="margin-top:25px;">
		<div class="col-md-4">
			<?= 
	            $form->field($model, "state")
		            ->dropDownList(
		                $states, 
		                [
		                	'prompt' => 'Selecciona...',
		                	'onChange' =>  '$.post("'.$urlTownIst.'"+"&id="+$(this).val(),function(data) {
                          		$("#gasolineprice-town").html(data);
                        	});'
		                ]
		            )->label("Estado");
	        ?>
		</div>
		<div class="col-md-4">
			<?= 
	            $form->field($model, "town")
		            ->dropDownList(
		                [], 
		                [
		                	'prompt' => 'Selecciona...',
		                ]
		            )->label("Municipio");
	        ?>
		</div>
		<div class="col-md-4">
			<?= 
	            $form->field($model, "order")
		            ->dropDownList(
		                $model->getOrderArray(),
		                [
		                	'prompt' => 'Selecciona...',
		                ]
		            )->label("Ordenar por precio");
	        ?>
		</div>
	</div>
	<div class="row">
		<div class="col-md-4">
			<?= 
	            $form->field($model, 'postal_code')
	            	->textInput(['maxlength' => 10, 'style' => 'width:350px'])
	            	->label('Código postal') 
	        ?>
		</div>
	</div>
	<div class ="row">
	    <div class="col-md-4 pull-left">
		    <?= Html::submitButton('Buscar', ['class' => 'btn btn-primary']) ?>    
		</div>
	</div>
<?php ActiveForm::end(); ?>

<div class ="row" style="margin-top:25px;">
	<div class="col-md-12" id="table_prices" >
	</div>
</div>