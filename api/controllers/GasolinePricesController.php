<?php

namespace api\controllers;

use Yii;
use yii\helpers\ArrayHelper;
use yii\data\ActiveDataProvider;

class GasolinePricesController extends \yii\rest\ActiveController {
    
    public $modelClass = 'api\\models\\GasolinePrice';
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        
        $behaviors['verbs'] = [
            'class' => \yii\filters\VerbFilter::class,
                'actions' => [
                    'index'  => ['GET'],
                    'view'   => ['GET'],
                    'create' => [],
                    'update' => [],
                    'delete' => []
                ],
            ];
        return $behaviors;
    }
    /**
     * @inheritdoc
     */
    public function actions(){
        return ArrayHelper::merge(
            parent::actions(),
            [
                'index' => [
                    'class' => 'api\actions\GasolinePrices',
                    'modelClass' => $this->modelClass,

                ]
            ]
        );
    }
}

