<?php
namespace frontend\controllers;

use frontend\models\GasolinePrice;
use Yii;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use common\models\CodigosPostales; 


use dosamigos\google\maps\LatLng;
use dosamigos\google\maps\services\DirectionsWayPoint;
use dosamigos\google\maps\services\TravelMode;
use dosamigos\google\maps\overlays\PolylineOptions;
use dosamigos\google\maps\services\DirectionsRenderer;
use dosamigos\google\maps\services\DirectionsService;
use dosamigos\google\maps\overlays\InfoWindow;
use dosamigos\google\maps\overlays\Marker;
use dosamigos\google\maps\Map;
use dosamigos\google\maps\services\DirectionsRequest;
use dosamigos\google\maps\overlays\Polygon;
use dosamigos\google\maps\layers\BicyclingLayer;


/**
 * Site controller
 */
class GasolinePriceController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['create', 'town-list', 'render'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['create','town-list', 'render'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actionCreate()
    {
        $model = new GasolinePrice();     
        
        return $this->render('create', [
            'model' => $model,
            'prices' => []
        ]);
    }

    public function actionRender()
    {
        $model = new GasolinePrice();     
        $request = Yii::$app->request;
        if ($request->isPost) {
            if($model->load(Yii::$app->request->post()) && $model->validate()){

                $prices = $model->getPrices($model);
                $pricesArray = json_decode($prices, true);

                $map = $this->points($pricesArray);

                return $this->renderAjax(
                    'prices',
                    ['prices' => $pricesArray, "map" => $map]
                );
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function actionTownList($id)
    {
         if (Yii::$app->request->isAjax) {
            $towns = CodigosPostales::find()
		        ->select(["c_mnpio", "municipio"])
		        ->where(["c_estado" => $id])
		        ->distinct()
		        ->all();
		
            if($towns != null) {
                    echo "<option value=''> Selecciona ...</option>";
                foreach ($towns as $town) {
                    echo "<option value='" . $town->c_mnpio . "'>" . $town->municipio . "</option>";
                }
            }  
        }
    }


    private function points($arrayPrices)
    {
        $map= [];
        foreach ($arrayPrices as $key => $value) {
            if ($key === 0) {
                $coord = new LatLng(['lat' => $value["latitude"], 'lng' => $value["longitude"]]);
                $map = new Map([
                    'center' => $coord,
                    'zoom' => 14
                ]);
            }
            $coord = new LatLng(['lat' => $value["latitude"], 'lng' => $value["longitude"]]);
            $marker = new Marker([
                'position' => $coord,
                'title' => $value["razonsocial"],
            ]);

            $marker->attachInfoWindow(
                new InfoWindow([
                    'content' => '<p>This is my super cool content</p>'
                ])
            );

            $map->addOverlay($marker);

        }
        return $map;
    }
}