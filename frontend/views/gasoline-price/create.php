<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Contrato */
$this->title = 'Buscar precios';
$this->params['breadcrumbs'][] = $this->title;
?>

<div>
    <?= Html::encode($this->title) ?> 

    <?= $this->render('_form', ['model' => $model, 'prices' => $prices]) ?>

</div>